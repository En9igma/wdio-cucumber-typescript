const allureResultsDir = './report/allure-results'

export const config: WebdriverIO.Config = {
    services: [
        [
            'selenium-standalone',
            {
                logs: 'logs',
            },
        ],
    ],
    specs: ['./features/**/*.feature'],
    baseUrl: 'https://webdriver.io/',
    waitforTimeout: 10000,
    maxInstances: 5,
    automationProtocol: 'webdriver',
    framework: 'cucumber',
    reporters: [
        'spec',
        [
            'allure',
            {
                outputDir: allureResultsDir,
                disableMochaHooks: true,
                useCucumberStepReporter: true,
                disableWebdriverStepsReporting: true,
            },
        ],
    ],
    cucumberOpts: {
        timeout: 180000,
        failFast: true,
        require: ['./test/step-definitions/**/*.ts'],
    },
    logLevel: 'info',
    outputDir: 'logs',
    // hooks
    before() {
        require('../src/wdio/addCommands')
    },
    async onPrepare() {
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        require('rimraf').sync(allureResultsDir)
    },
}
