import { When } from 'cucumber'

import { topNavigation } from '../elements/topNav'
import { documentNavigation } from '../elements/docNav'
import { onPageNavigation } from '../elements/pageNav'

When(/^I Open "([^"]*)" page from TOP navigation$/, (link: string) => {
    topNavigation.openByName(link)
})

When(/^I Open "([^"]*)" from DOC navigation$/, (link: string) => {
    documentNavigation.openByName(link)
})

When(/^I Open "([^"]*)" from PAGE navigation$/, (link: string) => {
    onPageNavigation.openByName(link)
})
